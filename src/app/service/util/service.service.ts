import { Injectable } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import {ToastrService} from "ngx-toastr";


@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private spinner: NgxSpinnerService, private toast: ToastrService) { }

  public showSpinner() {
    this.spinner.show();
  }

  public hideSpinner() {
    this.spinner.hide();
  }

  public toastOk(title: string, message: string) {
    this.toast.success(message, title);
  }

  public toastError(title: string, message: string) {
    this.toast.error(message, title);
  }

  public toastInfo(title: string, message: string) {
    this.toast.info(message, title);
  }

  public toastWarning(title: string, message: string) {
    this.toast.warning(message, title);
  }
}
