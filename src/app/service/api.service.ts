import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getAll(){
    return this.http.get(environment.urlBase + environment.getAll);
  }

  insert(email: string, estilos: string){
    let body = {
      email: email,
      estilosMusicales: estilos
    };
    return this.http.post(environment.urlBase + environment.insert, body);
  }
}
