import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ServiceService} from "../../service/util/service.service";
import {ApiService} from "../../service/api.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  emailForm: FormGroup;
  estilosList: string[] = ['Rock n Roll', 'Bachata', "Reggaeton", "Salsa", "Soul", "Reggae", "Merengue", "Metal", "Jazz", "Blues", "Clasica", "Gospel"];
  flag: boolean = false;
  estilosGuardados: string[] = [];
  newHistorial: any = [];

  constructor(private util: ServiceService, private fb: FormBuilder, private api: ApiService) {
    this.emailForm = this.fb.group({
      email: ["", [Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$'), Validators.required]],
      estilo: [""]
    });
  }

  ngOnInit(): void {

  }

  validForm(){
    this.emailForm.valid ? this.flag = true : this.flag = false;
    this.newHistorial = [];
  }

  enviarEncuesta(){
    this.util.showSpinner();
    if(this.flag){
      let list = this.estilosGuardados.join();
      this.api.insert(this.emailForm.controls['email'].value.trim(), list).subscribe(x =>{
        this.util.hideSpinner();
        this.util.toastOk('OK.', 'Datos ingresados correctamente');
        this.emailForm.reset();
        this.estilosGuardados = [];
        this.flag = false;
        this.getAll();
      }, err => {
        this.util.hideSpinner();
        this.util.toastError('Error!', err.message());
      });
    }
  }

  getAll(){
    this.util.showSpinner();
    this.api.getAll().subscribe(x =>{
      let historial : any = x;
      for(let i = 0; i < historial.length; i++){
        let body = {
          email: historial[i].email,
          estilosMusicales: historial[i].estilosMusicales.split(',')
        }
        this.newHistorial.push(body);
      }
      this.util.hideSpinner();
    }, err => {
      this.util.hideSpinner();
      this.util.toastError('Error!', err.message());
    });
  }

  addList(nombre: string){
    if(this.inList(nombre) == false){
      this.estilosGuardados.push(nombre);
      this.util.toastOk(nombre, 'Se agregó a la lista');
    }else{
      this.rmList(nombre);
    }
  }

  rmList(nombre: string){
    this.estilosGuardados = this.estilosGuardados.filter(x => x != nombre);
    this.util.toastError(nombre, 'Se quitó de la lista');
  }

  inList(nombre: string) {
    for(let i = 0; i < this.estilosGuardados.length; i++){
      if(this.estilosGuardados[i] == nombre){
        return true;
      }
    }
    return false;
  }

}
